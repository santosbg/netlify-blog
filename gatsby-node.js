const path = require('path')

module.exports.onCreateNode = ({ node, actions }) => {
  const { createNodeField } = actions;
  // Adding a new field to all the nodes - the slug
  // [TODO] Should pass if draft is true [TODO]
  if(node.internal.type === 'MarkdownRemark') {
    const slug = path.basename(node.fileAbsolutePath, '.md');

    createNodeField({
      node,
      name: 'slug',
      value: slug
    });
  }
};

module.exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const blogTempalte = path.resolve('./src/templates/blog-post.js');
  const res = await graphql(`
    query {
      allMarkdownRemark {
        edges {
          node {
            fields{
              slug
            }
          }
        }
      }
    }
  `);
  res.data.allMarkdownRemark.edges.forEach(edge => {
    createPage({
      component: blogTempalte,
      path: `/blog/${edge.node.fields.slug}`,
      context: {
        slug: edge.node.fields.slug
      }
    });
  });
};