import React from 'react';
import { Link, graphql, useStaticQuery } from 'gatsby';

import Layout from '../components/Layout';
import BlogPostCard from '../components/BlogPostCard';
import Seo from '../components/Seo';
import './../styles/common.scss'


const BlogPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allMarkdownRemark {
        edges {
          node {
            frontmatter {
              title
              date,
              firebase
            }
            fields {
              slug
            }
          }
        }
      }
    }
  `);

  const renderBlogsPosts = () => {
    return data.allMarkdownRemark.edges.map(edge => {
      const date = new Date(edge.node.frontmatter.date);
      const formatedDate = `${(date.getMonth() + 1)}/${date.getDate()}/${date.getFullYear()}`;

      return (
        <Link style={{ width: 'fit-content'}} key={edge.node.frontmatter.title} to={`/blog/${edge.node.fields.slug}`}>
          <BlogPostCard
            title={edge.node.frontmatter.title}
            image={edge.node.frontmatter.firebase}
            formatedDate={formatedDate}
          />
        </Link>
      );
    });
  };

  return (
    <Layout>
      <Seo />
      <div className="blog-posts-grid">
      {renderBlogsPosts()}
      </div>
    </Layout>
  );
};

export default BlogPage;