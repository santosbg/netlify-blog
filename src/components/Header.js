import React from 'react';
import { Link } from 'gatsby';

import './../styles/common.scss';

const Header = () => {
  return (
    <header className="header">
      <img className="logo" src="https://webassets.telerikacademy.com/images/default-source/logos/external-logos/techpods-logo.png?sfvrsn=37c54830_2" />
      <nav className="navigation">
        <Link className="link" activeClassName="active-link" to="/">Home</Link>
        <Link className="link" activeClassName="active-link" to="/blog">Blog</Link>
      </nav>
    </header>
  );
};

export default Header;