import React from 'react';

const BlogPostCard = ({ image, title, formatedDate}) => {

  return (
    <div className="blog-post-card">
        <img className="blog-post-card-image" src={image}/>
        <h2 className="blog-post-card-title">{title}</h2>
        <span className="blog-post-card-date">{formatedDate}</span>
    </div>
  );
};

export default BlogPostCard;