import React from 'react';
import { Helmet } from 'react-helmet';
import { useStaticQuery, graphql } from 'gatsby';

const Seo = ({ description, keywords, title, image, siteUrl, author }) => {
  const { site } = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title,
          image,
          siteUrl,
          description,
          author,
          keywords
        }
      }
    }
  `);

  // If no props use site seo
  const metaDescription = description || site.siteMetadata.description;
  const metaTitle = title || site.siteMetadata.title;
  const metaAuthor = author || site.siteMetadata.author;
  const metaImage = image || site.siteMetadata.image;
  const metaUrl = siteUrl || site.siteMetadata.siteUrl;
  const metaKeywords = keywords || site.siteMetadata.keywords;

  const metaData = [
    { property: 'og:title', content: metaTitle },
    { property: 'og:description', content: metaDescription },
    { property: 'og:type', content: metaUrl },
    { property: 'og:url', content: metaUrl },
    { property: 'og:type', content: metaImage },
    { name: 'description', content: metaDescription },
    { name: 'twitter:card', content: metaDescription },
    { name: 'twitter:title', content: metaTitle },
    { name: 'twitter:description', content: metaDescription },
    metaAuthor ? { name: 'twitter:creator', content: metaAuthor} : {}
  ].concat(
    metaKeywords && metaKeywords.length > 0 ? { name: 'keywords', content: metaKeywords.join(', ')} : []
  );

  return (
    <Helmet
      title={metaTitle}
      titleTemplate={`%s | ${metaTitle}`}
      meta={metaData}
    />
  )
};

export default Seo;