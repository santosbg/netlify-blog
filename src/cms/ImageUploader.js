// import React, { useState } from "react";
// import { storage } from "../utils/firebase";

// class ImageUploader extends React.Component({name, onChange, field, classNameWrapper, setActiveStyle, setInactiveStyle, forID }) =>{
//   constru

//   console.log(name)
//   const [progress, setProgress] = useState(0);
//   const [fileInput, setFileInput] = useState(null);

//   const handleNewImageInput = () => {
//     const file = fileInput.files[0];
//     let uploadTask = storage.ref().child(file.name).put(file);

//     uploadTask.on("state_changed",
//       snapshot => {
//         // Handle progress of upload
//         const progress = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100);
//         setProgress(progress);
//       },
//       error => {
//         // Handle unsuccessful upload
//       },
//       async () => {
//         // Handle successful upload
//         setProgress(progress);
//         const downloadURL = await uploadTask.snapshot.ref.getDownloadURL();
//         onChange(downloadURL)
//       }
//     );
//   }

//   return (
//       <input
//         className={classNameWrapper}
//         onFocus={setActiveStyle}
//         onBlur={setInactiveStyle}

//         id={forID}
//         type="file"
//         accept="image/*"
//         multiple={false}
//         ref={input => setFileInput(input)}
//         onChange={handleNewImageInput}
//       />
//   );
// }

// export default ImageUploader;


 
import PropTypes from 'prop-types';
import React from 'react';
import { storage } from "../utils/firebase";

export default class Control extends React.Component {
  constructor(props) {
    super(props);
    this.imageInput = React.createRef();
    this.state = {
      fileInput: null
    };
  }

  handleNewImageInput = () => {
    this.setState({ fileInput: this.imageInput.current})

    const {onChange } = this.props;
    if(this.state.fileInput){
      const file = this.state.fileInput.files[0];
      let uploadTask = storage.ref().child(file.name).put(file);

      uploadTask.on("state_changed",
        snapshot => {
          // Handle progress of upload
          const progress = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100);
        },
        error => {
          // Handle unsuccessful upload
        },
        async () => {
          // Handle successful upload
          const downloadURL = await uploadTask.snapshot.ref.getDownloadURL();
          onChange(downloadURL)
        }
      );
    }
  }

  // static propTypes = {
  //   onChange: PropTypes.func.isRequired,
  //   forID: PropTypes.string,
  //   value: PropTypes.node,
  //   classNameWrapper: PropTypes.string.isRequired,
  // }

  // static defaultProps = {
  //   value: '',
  // }

  render() {

    const {classNameWrapper, setActiveStyle, setInactiveStyle, forID } = this.props;


    return (
      <input
        className={classNameWrapper}
        onFocus={setActiveStyle}
        onBlur={setInactiveStyle}
        id={forID}
        type="file"
        accept="image/*"
        multiple={false}
        ref={this.imageInput}
        onChange={this.handleNewImageInput}
      />
    );
  }
}