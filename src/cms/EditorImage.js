import React from 'react';

const EditorImage = {
  // Internal id of the component
  id: "firebasei",
  // Visible label
  label: "FirebaseImage",
  // Fields the user need to fill out when adding an instance of the component
  fields: [{name: 'firebase', label: 'FirebaseImage', widget: 'firebase'}],
  // Pattern to identify a block as being an instance of this component
  pattern: /^firebasei (\S+)$/,
  // Function to extract data elements from the regexp match
  fromBlock: function(match) {
    return {
      id: match[1]
    };
  },
  // Function to create a text block from an instance of this component
  toBlock: function(obj) {
    console.log('toBlock', obj);
    return  `![Just a text](${obj.firebase})`;
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function(obj) {
    console.log('toPreview', obj)
    return (
      `![Just a text](${obj.firebase})`
    );
  }
};

export default EditorImage;