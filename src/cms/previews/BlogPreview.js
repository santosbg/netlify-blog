import React from 'react';

import '../../styles/index.scss';
import '../../styles/common.scss';

const BlogPreview = ({ entry, getAsset, widgetFor }) => {
  const image = entry.getIn(['data', 'image']);
  const imagePath = getAsset(image)

  const title = entry.getIn(['data', 'title']);

  const dateData = entry.getIn(['data', 'date']);
  const date = `${(dateData.getMonth() + 1)}/${dateData.getDate()}/${dateData.getFullYear()}`;

  const author = entry.getIn(['data', 'author']);
  
  const content = widgetFor('content');

  const image2 = entry.getIn(['data', 'firebase']);

  return (
    <div className="blog-container">
      <img className="blog-image" src={image2} alt={title}/>
      <h1 className="blog-title">{title}</h1>
      <div className="blog-about">
        <div className="info">
          <span>{author}</span>
          <span>{date.toString()}</span>
        </div>
        <button className="primary-btn">Share</button>
      </div>
      <div>{content}</div>
    </div>
  )
};

export default BlogPreview;