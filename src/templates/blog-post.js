import React from 'react';
import { graphql } from 'gatsby';
import Markdown from 'react-markdown'

import CodeBlock from '../components/CodeBlock';
import Layout from '../components/Layout';
import Seo from '../components/Seo';

import '../styles/index.scss';
import '../styles/common.scss';


export const query = graphql`
  query (
    $slug: String!
  ) {
    markdownRemark (
      fields: {
        slug: {
          eq: $slug
        }
      }
    ) {
      frontmatter {
        title,
        firebase,
        content,
        author,
        date,
        summary,
        tags
      },
      html
    }
  }
`;

const BlogPost = props => {
    const date = new Date(props.data.markdownRemark.frontmatter.date);
    const formatedDate = `${(date.getMonth() + 1)}/${date.getDate()}/${date.getFullYear()}`;

    const keywords = props.data.markdownRemark.frontmatter.tags[0].split(' ');
    console.log(props.data.markdownRemark.frontmatter.firebase)
    return (
      <Layout>
        <Seo 
          title={props.data.markdownRemark.frontmatter.title}
          keywords={keywords}
          description={props.data.markdownRemark.frontmatter.summary}
        />
        <div className="blog-container">
          <img
            className="blog-image"
            src={props.data.markdownRemark.frontmatter.firebase}
            alt={props.data.markdownRemark.frontmatter.title}
          />
          <h1
            className="blog-title"
          >{props.data.markdownRemark.frontmatter.title}</h1>
          <div className="blog-about">
            <div className="info">
              <span>{props.data.markdownRemark.frontmatter.author}</span>
              <span>{formatedDate}</span>
            </div>
            <button className="primary-btn">Share</button>
          </div>
          <div className="blog-content">
            <Markdown
              source={props.data.markdownRemark.frontmatter.content} 
              renderers={{ code: CodeBlock }}
            ></Markdown>
          </div>
        </div>
      </Layout>
    );
};

export default BlogPost;