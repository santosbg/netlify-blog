import firebase from 'firebase';
import 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyCz4bkp5JVcWpc-JpSxkTxsJ17LnQVGykg",
  authDomain: "website-new-6b648.firebaseapp.com",
  databaseURL: "https://website-new-6b648.firebaseio.com",
  projectId: "website-new-6b648",
  storageBucket: "website-new-6b648.appspot.com",
  messagingSenderId: "295691344703",
  appId: "1:295691344703:web:f3490366f46791ad65918a",
  measurementId: "G-WXY47MLGWD"
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}

const storage = firebase.storage();

export { storage };